package valluri.sriram.cipherssn;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class registration extends AppCompatActivity {

    public EditText mTeamName;
    public EditText mEntrynumber1, mEntrynumber2, mEntrynumber3;
    public EditText mName1, mName2, mName3;
    public String mTeamNameStr;
    public String mEntrynumberStr1, mEntrynumberStr2, mEntrynumberStr3;
    public String mNameStr1, mNameStr2, mNameStr3;
    public Button mSubmitButton;
    public View focusView;
    public boolean submit;
    public RequestQueue requestQueue;
    private static final String REGISTER_URL = "http://agni.iitd.ernet.in/cop290/assign0/register/";
    public Object responseMessege,responseSuccess;
    private static final String TAG = "REGISTRATION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        // sets text on ToolBar to string registration.title
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.tb_profile_activity);
        setSupportActionBar(mActionBarToolbar);
        getSupportActionBar().setTitle(R.string.registration_title);

        // Checks if the app is connected to internet or not
        submit = false;
        bindViews();
        setListeners();
        if(isOnline()){
            mSubmitButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    errorFocus();
		            if(submit){
                        registerTeam();
                    }
                }
            });
        }
        else{
            offlineAction();
        }
    }

    // Checks the internet connection of the user
    private boolean isOnline() {
        ConnectivityManager conMgr = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

        if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
            return false;
        }
        return true;
    }

    // Rasises a alert dialog if the user is offline
    private void offlineAction() {
        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(registration.this);
            alertDialog.setTitle("DEVICE OFFLINE");
            alertDialog.setMessage("Please check your connection");
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.setNegativeButton("close", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    // OnClicking it closes the dialog
                    dialog.cancel();
                }
            });
            alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // OnClicking it it goes to Settings
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                    dialog.cancel();
                }
            });
            alertDialog.show();
        } catch(Exception e) {
            Log.d("TAG", "Show Dialog: " + e.getMessage());
        }
    }

    /**
     * Method to bind xml views
     */
    private void bindViews() {
        mTeamName = (EditText) findViewById(R.id.teamName);

        mEntrynumber1 = (EditText) findViewById(R.id.entryNumber_1);
        mEntrynumber2 = (EditText) findViewById(R.id.entryNumber_2);
        mEntrynumber3 = (EditText) findViewById(R.id.entryNumber_3);

        mName1 = (EditText) findViewById(R.id.name_1);
        mName2 = (EditText) findViewById(R.id.name_2);
        mName3 = (EditText) findViewById(R.id.name_3);

        mSubmitButton = (Button) findViewById(R.id.button);
    }

    /**
     * Method to set listeners to views
     */
    private void setListeners() {
        mEntrynumber1.addTextChangedListener(textWatcherEntryNumber);
        mEntrynumber2.addTextChangedListener(textWatcherEntryNumber);
        mEntrynumber3.addTextChangedListener(textWatcherEntryNumber);
    }

    /**
     *  Checks the errors in Names
     */
    private void nameError(EditText name) {
        if ((name == null) || (name.length() < 1)) {
            name.setError(getString(R.string.empty_error_message));
            focusView = name;
            focusView.requestFocus();
            submit = false;
        }else if ((name.getText().toString().startsWith(" "))){
            name.setError(getString(R.string.not_allowed_error_message));
            focusView = name;
            focusView.requestFocus();
            submit = false;
        }else{
            submit = true;
        }
    }

    /**
     *  Checks the errors in EntryNumbers
     */
    private void entrynumberError(EditText entrynumber) {
        if ((entrynumber == null) || (entrynumber.length() < 1)) {
            entrynumber.setError(getString(R.string.empty_error_message));
            focusView = entrynumber;
            focusView.requestFocus();
            submit = false;
        } else if (entrynumber.length() < 11){
            entrynumber.setError(getString(R.string.in_sufficient_error_message));
            focusView = entrynumber;
            focusView.requestFocus();
            submit = false;
        } else if(!isDeptValid(entrynumber)){
            entrynumber.setError(getString(R.string.incorrect_entry_number));
            focusView = entrynumber;
            focusView.requestFocus();
            submit = false;
        } else{
            submit = true;
        }
    }

    /**
     * TextWatcher for checking the user input.
     * Uses textWatcher to capitalize the department of EntryNumber
     */
    private final TextWatcher textWatcherEntryNumber = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }
            @Override
            public void afterTextChanged(Editable et) {
                String s = et.toString();
                if (!s.equals(s.toUpperCase())) {
                     et.replace(0,et.length(),s.toUpperCase());
                }
            }
    };

    /**
     * @param entrynumber
     * Checks the deparment entered in EntryNumber is valid or not
     * @return boolean
     */
    private boolean isDeptValid (EditText entrynumber) {
        return entrynumber.getText().toString().substring(4,6).matches(getString(R.string.possible_dept_branches));
    }

    /**
     * Checks for errors in the name and entryNumber (editTexts)
     */
    private void errorFocus() {
        nameError(mTeamName);

        if(submit){ nameError(mName1);}
        if(submit){ entrynumberError(mEntrynumber1);}

        if(submit){ nameError(mName2);}
        if(submit){ entrynumberError(mEntrynumber2);}

        if(submit){
            if((mName3 == null) || (mName3.length() > 0 ) || (mEntrynumber3 == null) || (mEntrynumber3.length() > 0 )) {
                nameError(mName3);
                entrynumberError(mEntrynumber3);
            }
        }
    }

    /**
     * takes the entered team name , names & entry numbers
     * and returns a map with thier keys
     */
    private HashMap<String,String> ExtractViews(){
        HashMap<String,String> teamInfo = new HashMap<>();

        mTeamNameStr = mTeamName.getText().toString();
        mNameStr1 = mName1.getText().toString();
        mNameStr2 = mName2.getText().toString();
        mNameStr3 = mName3.getText().toString();

        mEntrynumberStr1 = mEntrynumber1.getText().toString();
        mEntrynumberStr2 = mEntrynumber2.getText().toString();
        mEntrynumberStr3 = mEntrynumber3.getText().toString();

        teamInfo.put("teamname", mTeamNameStr);

        teamInfo.put("entry1", mEntrynumberStr1);
        teamInfo.put("name1", mNameStr1);

        teamInfo.put("entry2", mEntrynumberStr2);
        teamInfo.put("name2", mNameStr2);

        teamInfo.put("entry3", mEntrynumberStr3);
        teamInfo.put("name3", mNameStr3);
        
        return teamInfo;
    }

    /**
     * This function will call the server using volley
     * and using POST method sends the parameters and
     * onresponse takes the string and converts it into JSON object
     */
    private void registerTeam(){
        requestQueue= Volley.newRequestQueue(this);
        final ProgressDialog processDialog = new ProgressDialog(this);
        processDialog.setMessage("Loading...");
        processDialog.show();

        StringRequest registrationRequest = new StringRequest(Request.Method.POST, REGISTER_URL, new Response.Listener<String>() {

            // onResponse from the server this function is called

            @Override
            public void onResponse(String response) {
                try {

                    //Converting the received response from server into JSONObject

                    JSONObject registrationResponse = new JSONObject(response);

                    responseMessege = registrationResponse.get(getString(R.string.response_message));
                    responseSuccess = registrationResponse.get(getString(R.string.response_success));

                    Log.d(TAG, responseMessege.toString());
                    Log.d(TAG, responseSuccess.toString());

                    processDialog.hide();

                    //Rasing a alertdialog depending on the response

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(registration.this);

                    switch (responseMessege.toString()){

                        case "Data not posted!":
                            alertDialog.setTitle(R.string.registration_unsuccessful);
                            alertDialog.setMessage(R.string.registration_unsuccessful_msg);
                            alertDialog.setIcon(android.R.drawable.ic_delete);
                            break;

                        case "Registration completed":
                            alertDialog.setTitle(R.string.registration_successful);
                            alertDialog.setMessage(R.string.registration_successful_msg);
                            alertDialog.setIcon(R.drawable.success);
                            break;

                        case "User already registered":
                            alertDialog.setTitle(R.string.error);
                            alertDialog.setMessage(R.string.user_already_registered_msg);
                            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                            break;

                    }
                    alertDialog.show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            // on error from the server this function is called

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                processDialog.hide();
            }
        }){

            //These parameters are send to the server

        @Override
        protected Map<String, String> getParams() {
            return ExtractViews();
        }
        };
        requestQueue.add(registrationRequest);
    }

}
